﻿var button1, button2, button3, button4;
var menuSlides = false;
var tween1;
var tween2;
var tween3;
var tween4;

$(document).ready(function ()
{
	//document.addEventListener("contextmenu", event => event.preventDefault());
	window.addEventListener("resize", onResize);
	button1 = document.getElementById("menu1");
	button1.addEventListener("click", function()
	{
		window.location="/Home";
	});
	button2 = document.getElementById("menu2");
	button2.addEventListener("click", function()
	{
		window.location="/Portfolio";
	});
	button3 = document.getElementById("menu3");
	button3.addEventListener("click", function()
	{
		window.location="/Blog";
	});
	button4 = document.getElementById("menu4");
	button4.addEventListener("click", function()
	{
		window.location="/Contacts";
	});
	
    var menuButton = document.getElementById("mobile-menu");
    if (device.mobile()) {
        menuButton.addEventListener('touchstart', function (event) {
			if (event.targetTouches.length === 1) {
				//var touch = event.targetTouches[0];
				//startPos = touch.pageY;
                HideMenu();
			}
		}, false);
                var social = document.getElementById("bottom");
		var logo = document.getElementById("head-block");		
		if(device.landscape())
                {
                    logo.style.top = -114 + "px";
                    social.style.bottom = -66 + "px";
                }
        	tween2 = KUTE.fromTo(logo, {translateY: 0}, {translateY: 114});
		tween1 = KUTE.fromTo(logo, {translateY: 114}, {translateY: 0});
                tween3 = KUTE.fromTo(social, {translateY: 0}, {translateY: -66});
		tween4 = KUTE.fromTo(social, {translateY: -66}, {translateY: 0});
		toMobileMenu();
	}
	else menuButton.addEventListener("click", toggleMenu);
    onResize();
});

function onResize()
{
	var height = $("#body").height();
        var page = window.location;
	var thumb = document.getElementById("thumb");
	var rect;
	var controller = page.pathname.split('/');
	switch(controller[1])
	{
		case "":
			rect = button1.getBoundingClientRect();
            if(device.mobile() && device.portrait())
                document.getElementById("img-container").style.marginTop=20 + "%";
		break;
		case "Home":
			rect = button1.getBoundingClientRect();
            if(device.mobile() && device.portrait())
                document.getElementById("img-container").style.marginTop=20 + "%";
		break;
		case "Portfolio":
			rect = button2.getBoundingClientRect();
		break;
		case "Blog":
			rect = button3.getBoundingClientRect();
		break;
		case "Contacts":
			rect = button4.getBoundingClientRect();
		break;
	}
	thumb.style.left=rect.left + "px";
	thumb.style.width=rect.width + "px";
	thumb.style.visibility = "visible";
	var heightLogo = document.getElementById("head-block").getBoundingClientRect().height;
	if (getComputedStyle(document.getElementById("thumb")).display !== "none") heightLogo+=66;
	if (!device.mobile())
        {
            document.getElementById("buffer").style.height = (height - heightLogo - 66) + "px";
        }
        else
    {
        var social = document.getElementById("bottom");
	var logo = document.getElementById("head-block");
        if(device.landscape())
        {
            logo.style.top = -114 + "px";
            social.style.bottom = -66 + "px";
            document.getElementById("proxi").style.top = 60 +"px";
        }
        else
        {
            logo.style.top = 0;
            social.style.bottom = 0;
            document.getElementById("proxi").style.top = 120 +"px";
        }
        document.getElementById("buffer").style.height = (height - 90) + "px";
    }
}

function toMobileMenu()
{
    document.getElementById("thumb").style.display = "none";
    document.getElementById("proxi").style.top = 120 + "px";
    var listBox = document.getElementById("list-box");
    listBox.style.position = "fixed";
    listBox.style.top = 40 + "px";
    listBox.style.paddingLeft = 0;
    listBox.style.backgroundColor="white";
    listBox.style.borderRadius = 10 + "px";
    listBox.style.display = "none";
    
    var items = document.getElementsByTagName("li");
    for (var i=0; i<items.length; i++)
    {
        items[i].style.float = "none";
        items[i].style.display = "flex";
        items[i].style.margin = 10 + "px";
        items[i].style.fontSize = 50 + "px";
    }
    var menu = document.getElementById("menu");
    menu.style.height = 0;
    menu.style.position = "fixed";
    menu.style.fontSize = 50 + "px";
    document.getElementById("mobile-menu").style.display = "inline-block";
    document.getElementsByClassName("master-1part")[0].style.float = "right";
}

function HideMenu()
{
    toggleMenu();
    if(!menuSlides && device.landscape())
    {
        menuSlides = true;
	    tween2.start();// step = 10;
        tween3.start();
	    setTimeout(function () {
            tween1.start();
            tween4.start();
            menuSlides = false;
        }, 3000);	
    }
}

function toggleMenu()
{
    $("#list-box").slideToggle(300, function () {
        if ($(this).css('display')  === "none")
            $(this).removeAttr("style");
    });
}
