﻿var array = [];

$(document).ready(function ()
{
	document.getElementById("remove").addEventListener("click", function()
	{
		showPreloader();
		var checkBoxes = $('input[type=checkbox]');
		for(var i=0; i<checkBoxes.length; i++)
		{
			if(checkBoxes[i].checked) array.push(parseInt(checkBoxes[i].id));
		}
		if(array.length === 0)
		{
			alert("Виберіть файли для видалення");
			return;
		}
		document.getElementById("arrRemove").value = JSON.stringify(array);
		document.getElementById("formRemove").submit();

	});
	document.getElementById("iframe").addEventListener("load", requestIsFinished);
	//document.getElementById("main-body").addEventListener("click", closeIframe);
});

function showPreloader()
{
	document.getElementById("progress").style.visibility = "visible";
}

function hidePreloader()
{
	document.getElementById("progress").style.visibility = "hidden";
}

function requestIsFinished()
{
	hidePreloader();
	var element = document.getElementById("iframe");
	element.style.visibility = "visible";
	element.style.zIndex = 100;
	var iframe = $('#iframe').contents();
	iframe.find("body").click(function(){
	   closeIframe();
	});
}

function closeIframe()
{
	var element = document.getElementById("iframe");
	element.style.visibility = "hidden";
	element.style.zIndex = 0;
}