var images = [];
var currentImg;

$(document).ready(function ()
{
    $('div.img-container').children().each(function () {
        if (this.tagName === "IMG") images.push(this.id);
    });
    currentImg = 0;
    loadImg();
});

function loadImg()
{
    //var myData = JSON.stringify({json: JSON.stringify({Id:images[currentImg],Width:$(document).width()})});
    var myData = {id:parseInt(images[currentImg]),width:$(document).width()};
    $.ajax({
        type: "POST", //GET or POST or PUT or DELETE verb
        url: "/Portfolio/LoadImage", // Location of the service
        contentType: "application/json; charset=utf-8", // content type sent to server
        dataType: "json", //Expected data format from server
        data: JSON.stringify(myData),
        timeout:15000,
        processdata: true, //True or False
        success: function(result) {//On Successfull service call
            ImgLoadedSuccess(result);
        },
        error: ServiceFailed// When Service call fails
    });
}

function ImgLoadedSuccess(result)
{
    document.getElementById(images[currentImg]).setAttribute("src", "data:image/jpeg;base64," + result);
    if(images.length> ++currentImg) loadImg();
}

function ServiceFailed(error, userContext, methodName)
{
    if(images.length> ++currentImg) loadImg();
}