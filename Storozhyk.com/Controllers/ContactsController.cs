﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Storozhyk.com.Controllers
{
    public class ContactsController : Controller
    {
        public ActionResult Index()
        {
            if(!Commons.Instance.CheckUser(Session, Request)) return RedirectToAction("Index", "BadGateway");
            return View ();
        }
    }
}
