﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using RomBerSoft.Logging;
using System.Threading.Tasks;

namespace Storozhyk.com.Controllers
{
    public class PortfolioController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            if(!Commons.Instance.CheckUser(Session, Request)) return RedirectToAction("Index", "BadGateway");
            PicturesDataStorage storage = new PicturesDataStorage();
            PortfolioDataStorage portfolioDb = new PortfolioDataStorage();
            Portfolio[] portfolios = portfolioDb.LoadPreViews();
            uint[] idPic = new uint[portfolios.Length];
            for (short i = 0; i < portfolios.Length; i++)
            {
                idPic[i] = portfolios[i].IdMainImage;
            }
            Picture[] pictures = storage.LoadImages(idPic, 600);
            for (short i = 0; i < portfolios.Length; i++)
            {
                Picture current = pictures.FirstOrDefault(x => x.Id == portfolios[i].IdMainImage);
                if(current == null) continue;
                portfolios[i].MainImage = Convert.ToBase64String(current.AloneBody);
            }
            return View (portfolios);
        }
        
        [HttpGet]
        public ActionResult Watch(uint id, int width)
        {
            if(!Commons.Instance.CheckUser(Session, Request)) return RedirectToAction("Index", "BadGateway");
            PortfolioDataStorage portfolioDs = new PortfolioDataStorage();
            Portfolio portfolio = portfolioDs.Load(Convert.ToUInt16(id));
            if (portfolio == null) return RedirectToAction("Index", "BadGateway");
            Commons.Instance.FixWidth(ref width);
            Task.Run(() =>
                {
                    PicturesDataStorage storage = new PicturesDataStorage();
                    Picture[] images = storage.LoadImages(portfolio.Images, width);
                    Logs.Debug("Start upload to redis");
                    Dictionary<string, byte[]> dictionary = new Dictionary<string, byte[]>(images.Length);
                    for(byte i=0; i<images.Length; i++)
                    {
                        dictionary.Add(images[i].Id.ToString() + "_" + width, images[i].AloneBody);
                    }
                    Commons.Instance.AddToRedis(dictionary);
                    Logs.Debug("Finish upload to redis");
                });
            return View(portfolio);
        }

        [HttpPost]
        public JsonResult LoadImage(uint id, int width)
        {
            //if(!Commons.Instance.CheckUser(Session, Request)) return RedirectToAction("Index", "BadGateway");
            Commons.Instance.FixWidth(ref width);
            byte[] buff = Commons.Instance.GetFromRedis(id.ToString() + "_" + width);
            if (buff == null)
            {
                buff = new PicturesDataStorage().LoadImage(id, width);
                Commons.Instance.AddToRedis(id.ToString() + "_" + width, buff);
            }
            return Json(Convert.ToBase64String(buff));
        }
    }
}
