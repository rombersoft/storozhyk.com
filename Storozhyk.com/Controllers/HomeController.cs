﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

namespace Storozhyk.com.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if(!Commons.Instance.CheckUser(Session, Request)) return RedirectToAction("Index", "BadGateway");
            try
            {
                ViewData["Slide"] = Convert.ToBase64String(Commons.Instance.CurrentSlide);
            }
            catch(Exception){
            }
            return View();
        }
    }
}

