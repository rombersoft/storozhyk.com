﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Drawing;
using MySql.Data.MySqlClient;
using System.Text;
using Newtonsoft.Json;
using RomBerSoft.Logging;
using System.Drawing.Imaging;
using Gdk;
using Point = System.Drawing.Point;

namespace Storozhyk.com.Controllers
{
    public class AdminToolkitController : Controller
    {
        public ActionResult Index(string adminResults)
        {
            if(!Commons.Instance.CheckUser(Session, Request)) return RedirectToAction("Index", "BadGateway");
            if (Session["Login"] == null || Session["Password"] == null)
                return RedirectToAction("Authorization", "AdminToolkit");
            else
            {
                if (Session["TryAuthorize"] != null && (int)(Session["TryAuthorize"]) == 6)
                    return RedirectToAction("Index", "BadGateway");
            }
            ViewData["PreView"] = Commons.Instance.CreatePreView();
            AdminResults results;
            if (adminResults == null)
            {
                results = new AdminResults();
                results.Ok = new string[0];
                results.Fail = new string[0];
            }
            else
                results = JsonConvert.DeserializeObject<AdminResults>(adminResults);
            return View(results);
        }

        [HttpPost]
        public ActionResult LoadFile()
        {
            if(!Commons.Instance.CheckUser(Session, Request)) return RedirectToAction("Index", "BadGateway");
            if (Session["Login"] == null || Session["Password"] == null) return RedirectToAction("Authorization", "AdminToolkit");
            List<string> listOk = new List<string>();
            List<string> listFail = new List<string>();
            Image authorSymbol = Image.FromFile(Server.MapPath("~/Content/4r32q45lrek3tet34vase.png"));
            SlidesDataStorage storage = null;
            if(Request.Files.Count > 0) storage = new SlidesDataStorage();
            for(int i=0; i< Request.Files.Count; i++)
            {
                string fileName = Request.Files[i].FileName;
                if (Request.Files[i].ContentType == "image/jpg" || Request.Files[i].ContentType == "image/jpeg" || Request.Files[i].ContentType == "image/png")
                {
                    try
                    {
                        byte[] buff1, buff2;
                        using (MemoryStream stream = new MemoryStream())
                        {
                            using (Image img = Image.FromStream(Request.Files[i].InputStream))
                            {
                                int width = 600 * img.Width / img.Height;
                                using (Image imgResized = Commons.Instance.ScaleImage(img, width, 600))
                                {
                                    using (Bitmap bit1 = new Bitmap(width, 600))
                                    {
                                        using (Graphics g = Graphics.FromImage(bit1))
                                        {
                                            g.Clear(System.Drawing.Color.White);
                                            g.DrawImage(imgResized, new System.Drawing.Point(0, 0));
                                            g.DrawImage(authorSymbol, new System.Drawing.Point(imgResized.Width - authorSymbol.Width - 20, imgResized.Height - authorSymbol.Height - 20));
                                        }
                                        bit1.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
                                    }
                                    buff1 = stream.ToArray();
                                    using (Image preView = Commons.Instance.ScaleImage(imgResized, 100 * imgResized.Width / imgResized.Height, 100))
                                    {
                                        using(MemoryStream stream2 = new MemoryStream())
                                        {
                                            preView.Save(stream2, System.Drawing.Imaging.ImageFormat.Jpeg);
                                            buff2 = stream2.ToArray();
                                        }
                                    }
                                }
                            }
                        }
                        if(storage.Save(buff1,buff2)) listOk.Add(fileName);
                        else listFail.Add(fileName);
                    }
                    catch (Exception ex)
                    {
                        Logs.Error(ex.ToString());
                        listFail.Add(fileName);
                    }
                }
                else
                {
                    listOk.Add(fileName);
                }
                Request.Files[i].InputStream.Dispose();
            }
            authorSymbol.Dispose();
            AdminResults results = new AdminResults();
            results.Ok = listOk.ToArray();
            results.Fail = listFail.ToArray();
            return RedirectToAction("Index", "AdminToolkit", new {adminResults = results.ToString()});
        }

        public ActionResult Authorization(string id)
        {
            if(!Commons.Instance.CheckUser(Session, Request)) return RedirectToAction("Index", "BadGateway");
            ViewData["Authorization"] = id;
            return View(new User());
        }

        [HttpPost]
        public ActionResult CheckUser(User user)
        {
            if(!Commons.Instance.CheckUser(Session, Request)) return RedirectToAction("Index", "BadGateway");
            if (Session["TryAuthorize"] == null)
                Session["TryAuthorize"] = 1;
            else
                Session["TryAuthorize"] = (int)(Session["TryAuthorize"]) + 1;
            if ((int)(Session["TryAuthorize"]) >= 6)
                return RedirectToAction("Index", "BadGateway");
            if (!Commons.Instance.VerifyAdmin(user))
            {
                return RedirectToAction("Authorization", "AdminToolkit", new { id = "Login or password is wrong"});
            }
            else
            {
                Session["Login"] = user.Login;
                Session["Password"] = user.Password;
                Session["TryAuthorize"] = null;
                return RedirectToAction("Index", "AdminToolkit");
            }
        }

        [HttpPost]
        public ActionResult RemoveFile(string text)
        {
            if(!Commons.Instance.CheckUser(Session, Request)) return RedirectToAction("Index", "BadGateway");
            AdminResults results = new AdminResults();
            if (Session["Login"] == null || Session["Password"] == null)
                return RedirectToAction("Authorization", "AdminToolkit");
            uint[] iDs = JsonConvert.DeserializeObject<uint[]>(text);
            if (iDs.Length > 0)
            {
                long result = new SlidesDataStorage().Remove(iDs);
                results.Ok = new string[1]{ result.ToString() };
                results.Fail = new string[1]{ (iDs.Length - result).ToString() };
                return RedirectToAction("Index", "AdminToolkit", new {adminResults = results.ToString()});
            }
            else
            {
                results.Ok = new string[]{ "0" };
                results.Fail = new string[]{ "0" };
            }
            return RedirectToAction("Index", "AdminToolkit", new {adminResults = results.ToString()});
        }

        public PartialViewResult ImagePreview()
        {
            return PartialView(Commons.Instance.GetPreViews());
        }

        [HttpPost]
        public PartialViewResult UploadBlog(string title, string text, string video)
        {
            if (Request.Files == null || Request.Files.Count == 0 || string.IsNullOrEmpty(Request.Files[0].FileName))
            {
                ViewData["result"] = "Блог <b>" + title + "</b> не може бути доданий без картинок";
                return PartialView();
            }
            Image authorSymbol = Image.FromFile(Server.MapPath("~/Content/4r32q45lrek3tet34vase.png"));
            List<string> listOk = new List<string>();
            List<string> listFail = new List<string>();
            List<uint> idPicList = new List<uint>();
            PicturesDataStorage storage = new PicturesDataStorage();
            for (int i = 0; i < Request.Files.Count; i++)
            {
                string fileName = Request.Files[i].FileName;
                if (Request.Files[i].ContentType == "image/jpg" || Request.Files[i].ContentType == "image/jpeg" ||
                    Request.Files[i].ContentType == "image/png")
                {
                    try
                    {
                        byte[] buff1000 = Commons.Instance.RemakeImg(Request.Files[i].InputStream, authorSymbol, 1000);
                        byte[] buff800 = Commons.Instance.RemakeImg(Request.Files[i].InputStream, authorSymbol, 800);
                        byte[] buff600 = Commons.Instance.RemakeImg(Request.Files[i].InputStream, authorSymbol, 600);
                        uint result = storage.Store(buff1000, buff800, buff600);
                        if (result > 0)
                        {
                            listOk.Add(fileName);
                            idPicList.Add(result);
                        }
                        else listFail.Add(fileName);
                    }
                    catch (Exception ex)
                    {
                        Logs.Error(ex.ToString());
                        listFail.Add(fileName + " - " + ex.Message);
                    }
                }
                else
                {
                    listOk.Add(fileName);
                }
                Request.Files[i].InputStream.Dispose();
            }
            authorSymbol.Dispose();
            if (listOk.Count == Request.Files.Count)
            {
                BlogsDataStorage blogStorage = new BlogsDataStorage();
                Blog blog = new Blog();
                blog.IdMainImage = idPicList[0];
                blog.Images = idPicList.ToArray();
                blog.Title = title;
                if (text.Length > 255) blog.PreviewText = text.Substring(0, 255);
                else blog.PreviewText = text;
                string[] blocks = text.Replace("\r", "").Trim().Split('\n');
                StringBuilder builder = new StringBuilder(1500);
                for (byte i = 0; i < blocks.Length; i++)
                {
                    builder.Append("<p class=\"block-text\">");
                    builder.Append(blocks[i]);
                    builder.Append("</p>");
                }
                blog.Text = builder.ToString();
                if (blogStorage.Store(blog))
                {
                    ViewData["result"] = "Блог <b>" + title + "</b> успішно доданий";
                    return PartialView();
                }
                else
                {
                    ViewData["result"] = "Блог <b>" + title + "</b> не доданий. Помилка збереження в таблицю Blog";
                    return PartialView();
                }
            }
            ViewData["result"] = "Блог <b>" + title + "</b> не доданий. Не збереглися такі фотографії: " + string.Join("\n", listFail);
            return PartialView();
        }
        
        [HttpPost]
        public PartialViewResult UploadPortfolio(string title, string text, string video)
        {
            if (Request.Files == null || Request.Files.Count == 0 || string.IsNullOrEmpty(Request.Files[0].FileName))
            {
                ViewData["result"] = "Портфоліо <b>" + title + "</b> не може бути додане без картинок";
                return PartialView();
            }
            Image authorSymbol = Image.FromFile(Server.MapPath("~/Content/4r32q45lrek3tet34vase.png"));
            List<string> listOk = new List<string>();
            List<string> listFail = new List<string>();
            List<uint> idPicList = new List<uint>();
            PicturesDataStorage storage = new PicturesDataStorage();
            for(int i=0; i< Request.Files.Count; i++)
            {
                string fileName = Request.Files[i].FileName;
                if (Request.Files[i].ContentType == "image/jpg" || Request.Files[i].ContentType == "image/jpeg" || Request.Files[i].ContentType == "image/png")
                {
                    try
                    {
                        byte[] buff1000 = Commons.Instance.RemakeImg(Request.Files[i].InputStream, authorSymbol, 1000);
                        byte[] buff800 = Commons.Instance.RemakeImg(Request.Files[i].InputStream, authorSymbol, 800);
                        byte[] buff600 = Commons.Instance.RemakeImg(Request.Files[i].InputStream, authorSymbol, 600);
                        uint result = storage.Store(buff1000, buff800, buff600);
                        if(result > 0)
                        {
                            listOk.Add(fileName);
                            idPicList.Add(result);
                        }
                        else listFail.Add(fileName);
                    }
                    catch (Exception ex)
                    {
                        Logs.Error(ex.ToString());
                        listFail.Add(fileName + " - " + ex.Message);
                    }
                }
                else
                {
                    listOk.Add(fileName);
                }
                Request.Files[i].InputStream.Dispose();
            }
            authorSymbol.Dispose();
            if (listOk.Count == Request.Files.Count)
            {
                PortfolioDataStorage portfolioStorage = new PortfolioDataStorage();
                Portfolio portfolio = new Portfolio();
                portfolio.IdMainImage = idPicList[0];
                portfolio.Images = idPicList.ToArray();
                portfolio.Title = title;
                string[] blocks = text.Replace("\r", "").Trim().Split('\n');
                StringBuilder builder = new StringBuilder(1500);
                for (byte i = 0; i < blocks.Length; i++)
                {
                    builder.Append("<p class=\"block-text\">");
                    builder.Append(blocks[i]);
                    builder.Append("</p>");
                }
                portfolio.Text = builder.ToString();
                portfolio.Video = video;
                if (portfolioStorage.Store(portfolio))
                {
                    ViewData["result"] = "Портфоліо <b>" + title + "</b> успішно додане";
                    return PartialView();
                }
                else
                {
                    ViewData["result"] = "Портфоліо <b>" + title + "</b> не доданийе. Помилка збереження в таблицю Blog";
                    return PartialView();
                }
            }
            ViewData["result"] = "Портфоліо <b>" + title + "</b> не додане. Не збереглися такі фотографії: " + string.Join("\n", listFail);
            return PartialView();
        }
    }
}
