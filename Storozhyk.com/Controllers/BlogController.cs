﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Drawing;
using System.Threading.Tasks;
using RomBerSoft.Logging;

namespace Storozhyk.com.Controllers
{
    public class BlogController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            if(!Commons.Instance.CheckUser(Session, Request)) return RedirectToAction("Index", "BadGateway");
            PicturesDataStorage storage = new PicturesDataStorage();
            BlogsDataStorage blogDS = new BlogsDataStorage();
            Blog[] blogs = blogDS.LoadPreViews();
            uint[] idPic = new uint[blogs.Length];
            for (short i = 0; i < blogs.Length; i++)
            {
                idPic[i] = blogs[i].IdMainImage;
            }
            Picture[] pictures = storage.LoadImages(idPic, 600);
            for (short i = 0; i < blogs.Length; i++)
            {
                Picture current = pictures.FirstOrDefault(x => x.Id == blogs[i].IdMainImage);
                if(current == null) continue;
                blogs[i].MainImage = Convert.ToBase64String(current.AloneBody);
            }
            return View(blogs);
        }

        [HttpGet]
        public ActionResult Watch(uint id, int width)
        {
            //if(!Commons.Instance.CheckUser(Session, Request)) return RedirectToAction("Index", "BadGateway");
            BlogsDataStorage blogDs = new BlogsDataStorage();
            Blog blog = blogDs.Load(Convert.ToUInt16(id));
            if (blog == null) return RedirectToAction("Index", "BadGateway");
            Commons.Instance.FixWidth(ref width);
            Task.Run(() =>
                {
                    PicturesDataStorage storage = new PicturesDataStorage();
                    Picture[] images = storage.LoadImages(blog.Images, width);
                    Dictionary<string, byte[]> dictionary = new Dictionary<string, byte[]>(images.Length);
                    for(byte i=0; i<images.Length; i++)
                    {
                        dictionary.Add(images[i].Id.ToString() + "_" + width, images[i].AloneBody);
                    }
                    Commons.Instance.AddToRedis(dictionary);
                });
            return View(blog);
        }
    }
}