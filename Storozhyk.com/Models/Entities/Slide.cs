﻿using System;

namespace Storozhyk.com
{
    public class Slide
    {
        public uint Id{ get; set;}
        public string PreView { get; set;}

        public Slide(uint id, byte[] buff)
        {
            Id = id;
            PreView = Convert.ToBase64String(buff);
        }
    }
}

