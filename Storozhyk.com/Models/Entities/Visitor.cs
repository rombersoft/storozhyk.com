﻿using System;
using Newtonsoft.Json;

namespace Storozhyk.com
{
    public class Visitor
    {
        public Visitor(string ip, string date, string webEngine)
        {
            IP = ip;
            Date = date;
            WebEngine = webEngine;
        }

        public string IP {get;set;}
        public string Date {get;set;}
        public string WebEngine {get;set;}

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}

