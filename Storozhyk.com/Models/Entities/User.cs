﻿using System;
using Newtonsoft.Json;

namespace Storozhyk.com
{
    public class User
    {
        public string Login { get; set; }

        public string Password { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}

