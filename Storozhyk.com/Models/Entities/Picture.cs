﻿using System;

namespace Storozhyk.com
{
    public class Picture
    {
        public uint Id { get; set;}
        public byte[] Body1000 { get; set;}
        public byte[] Body800 { get; set; }
        public byte[] Body600 { get; set; }
        public byte[] AloneBody { get; set;}

        public Picture(uint id, byte[] body)
        {
            Id = id;
            AloneBody = body;
        }

        public Picture(uint id, byte[] body1000, byte[] body800, byte[] body600)
        {
            Id = id;
            Body1000 = body1000;
            Body800 = body800;
            Body600 = body600;
        }
    }
}

