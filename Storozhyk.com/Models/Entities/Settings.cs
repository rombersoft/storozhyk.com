﻿using System;
using Newtonsoft.Json;

namespace Storozhyk.com
{
    public class Settings
    {
        public uint LastSlideIndex { get;set; }
        public byte ImageQuality { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}

