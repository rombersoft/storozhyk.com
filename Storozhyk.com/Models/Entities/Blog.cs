﻿using System;

namespace Storozhyk.com
{
    public class Blog
    {
        public ushort Id { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public uint IdMainImage { get; set; }
        public string Date { get; set; }
        public uint[] Images { get; set; }
        public string MainImage { get; set; }
        public string PreviewText { get;set; }
        public string Video { get; set; }
    }
}

