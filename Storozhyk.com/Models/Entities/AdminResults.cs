﻿using System;
using Newtonsoft.Json;

namespace Storozhyk.com
{
    public class AdminResults
    {
        public string[] Ok {get;set;}
        public string[] Fail {get;set;}

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}

