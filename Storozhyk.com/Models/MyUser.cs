﻿using System;
using System.Collections.Generic;

namespace Storozhyk.com
{
    public class MyUser
    {
        public string Ident { get; set; }
        
        public List<DateTime> Activies { get; set; }

        public MyUser(string ident)
        {
            Ident = ident;
            Activies = new List<DateTime>(10);
            Activies.Add(DateTime.Now);
        }
    }
}