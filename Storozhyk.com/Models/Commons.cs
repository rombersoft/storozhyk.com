﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using Newtonsoft.Json;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using Gdk;
using RomBerSoft.Logging;
using System.Threading.Tasks;
using System.Web;
using RedisBoost;
using System.Collections.Generic;

namespace Storozhyk.com
{
    public class Commons
    {
        private User _user;
        private MySqlProvider _dbProvider;
        private static Commons _instance;
        private Scheduler _scheduler;
        private BlockingCollection<MyUser> _users;
        private Task _task;
        private IRedisClientsPool _redisPool;
        public Settings Settings { get; set; }

        public static Commons Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new Commons();
                return _instance;
            }
        }

        private Commons()
        {
            _dbProvider = new MySqlProvider();
            string sql = "SELECT Body FROM Settings WHERE `Key` = 'User'";
            object obj = _dbProvider.ScalarQuery(sql);
            if (obj != null)
                _user = JsonConvert.DeserializeObject<User>(obj.ToString());
            _scheduler = new Scheduler();
            _users = new BlockingCollection<MyUser>(1000);
            Settings = new SettingsDataStorage().Load<Settings>();
            _task = new Task(ClearActivies);
            _task.Start();
            _redisPool = RedisClient.CreateClientsPool();
        }

        public bool VerifyAdmin(User user)
        {
            return _user.Login == user.Login && _user.Password == user.Password;
        }

        public Image ScaleImage(Image source, int width, int height)
        {

            Image dest = new Bitmap(width, height);
            using (Graphics gr = Graphics.FromImage(dest))
            {
                gr.FillRectangle(Brushes.White, 0, 0, width, height); // Очищаем экран
                gr.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;

                float srcwidth = source.Width;
                float srcheight = source.Height;
                float dstwidth = width;
                float dstheight = height;

                if (srcwidth <= dstwidth && srcheight <= dstheight) // Исходное изображение меньше целевого
                {
                    int left = (width - source.Width) / 2;
                    int top = (height - source.Height) / 2;
                    gr.DrawImage(source, left, top, source.Width, source.Height);
                }
                else if (srcwidth / srcheight > dstwidth / dstheight) // Пропорции исходного изображения более широкие
                {
                    float cy = srcheight / srcwidth * dstwidth;
                    float top = ((float) dstheight - cy) / 2.0f;
                    if (top < 1.0f) top = 0;
                    gr.DrawImage(source, 0, top, dstwidth, cy);
                }
                else // Пропорции исходного изображения более узкие
                {
                    float cx = srcwidth / srcheight * dstheight;
                    float left = ((float) dstwidth - cx) / 2.0f;
                    if (left < 1.0f) left = 0;
                    gr.DrawImage(source, left, 0, cx, dstheight);
                }
                return dest;
            }
        }

        public byte[] CurrentSlide
        {
            get { return _scheduler.CurrentSlide; }
        }

        public void AddVisitors(string ip, string webEngine)
        {
            new VisitorsDataStorage().Save(new Visitor(ip, DateTime.Now.ToString("yyyy-MM-dd HH:mm"), webEngine));
        }

        public Slide[] GetPreViews()
        {
            try
            {
                return new SlidesDataStorage().LoadPreViews();
            }
            catch (ObjectNotFoundException)
            {
                return new Slide[0];
            }
        }

        public string CreatePreView()
        {
            StringBuilder preViewData = new StringBuilder(5000);
            Slide[] slides = Commons.Instance.GetPreViews();
            for (int i = 0; i < slides.Length; i++)
            {
                preViewData.Append("<div class=\"div-preView\">");
                preViewData.Append(Environment.NewLine);
                preViewData.Append("<img src = 'data:image/png;base64,");
                preViewData.Append(slides[i].PreView);
                preViewData.Append("'>");
                preViewData.Append(Environment.NewLine);
                preViewData.Append("<input id=\"");
                preViewData.Append(slides[i].Id);
                preViewData.Append("\" type=\"checkbox\" name=\"cb\" class=\"center\"/>");
                preViewData.Append(Environment.NewLine);
                preViewData.Append("</div>");
            }
            return preViewData.ToString();
        }

        public byte[] RemakeImg(Stream stream, Image logo, int width)
        {
            byte[] buff;
            Image img = Image.FromStream(stream);
            if (img.Width == width && stream is MemoryStream) return (stream as MemoryStream).ToArray();
            using (MemoryStream memStream = new MemoryStream())
            {
                if (img.Width != width)
                {
                    int height = width * img.Height / img.Width;
                    img = ScaleImage(img, width, height);
                }
                if (logo != null)
                {
                    using (Graphics g = Graphics.FromImage(img))
                        g.DrawImage(logo, new System.Drawing.Point(img.Width - logo.Width - 20, img.Height - logo.Height - 20));
                }

                img.Save(memStream, ImageFormat.Bmp);
                using (Pixbuf result = new Pixbuf(memStream.ToArray()))
                {
                    buff = result.SaveToBuffer("jpeg", new string[] {"quality"}, new string[] {Settings.ImageQuality.ToString()});
                }
            }
            img.Dispose();
            return buff;
        }

        public void FixWidth(ref int width)
        {
            if (width < 750)  width = 600;
            else if (width < 950) width = 800;
            else width = 1000;
        }

        public bool CheckUser(HttpSessionStateBase session, HttpRequestBase request)
        {
            if(string.IsNullOrEmpty(request.UserHostAddress) || string.IsNullOrEmpty(request.UserAgent) || request.UserAgent.Contains("zgrab")
               || request.UserAgent.Contains("nutch"))
            {
                return false;
            }
            string ident = request.ServerVariables["REMOTE_HOST"] + ":" + request.ServerVariables["REMOTE_PORT"];
            if (request.ServerVariables["REMOTE_HOST"] != "127.0.0.1" && session["client"] == null)
            {
                session["client"] = ident;
                string device = request.Browser.IsMobileDevice ? "mobile " : "computer ";
                Commons.Instance.AddVisitors(ident, device + request.UserAgent);
            }
            MyUser currentUser = _users.FirstOrDefault(x => x.Ident == ident);
            if (currentUser != null)
            {
                if (currentUser.Activies.Count > 199 && (DateTime.Now - currentUser.Activies.Last()).TotalSeconds < 10)
                {
                    Logs.Error(ident + " try to fall site");
                    return false;
                }
                else currentUser.Activies.Add(DateTime.Now);
            }
            else _users.TryAdd(new MyUser(ident));
            return true;
        }

        private void ClearActivies()
        {
            while (true)
            {
                for (int i = 0; i < _users.Count; i++)
                {
                    MyUser user = _users.ElementAt(i);
                    if (user.Activies.Count > 5) user.Activies.RemoveRange(0, 6);
                    else
                    {
                        if (_users.TryTake(out user)) i--;
                    }
                }
                System.Threading.Thread.Sleep(60000);
            }
        }
        
        public async void AddToRedis(string key, byte[] value)
        {
            using (IRedisClient client = _redisPool.CreateClientAsync("data source=127.0.0.1:6379;initial catalog=1").Result)
            {
                long result = await client.ExistsAsync(key);
                if (result == 0) client.SetExAsync(key, TimeSpan.FromMinutes(10), value).Wait();
            }
        }

        public async void AddToRedis(Dictionary<string, byte[]> dictionary)
        {
            using (IRedisClient client = _redisPool.CreateClientAsync("data source=127.0.0.1:6379;initial catalog=1").Result)
            {
                foreach (string key in dictionary.Keys)
                {
                    long result = await client.ExistsAsync(key);
                    if (result == 0) client.SetExAsync(key, TimeSpan.FromMinutes(10), dictionary[key]).Wait();
                }
            }
        }

        public byte[] GetFromRedis(string key)
        {
            using (IRedisClient client = _redisPool.CreateClientAsync("data source=127.0.0.1:6379;initial catalog=1").Result)
            {
                Task<Bulk> task = client.GetAsync(key);
                task.Wait();
                task.Dispose();
                return task.Result.Value;
            }
        }
    }
}

