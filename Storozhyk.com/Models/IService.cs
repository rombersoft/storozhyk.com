﻿using System;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace Storozhyk.com
{
    [ServiceContract(Namespace = "MyService")]
    public interface IService
    {
        [OperationContract]
        [WebGet(BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        string[] GetNamesPhoto();

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        byte SendEmail(string json);
    }
}