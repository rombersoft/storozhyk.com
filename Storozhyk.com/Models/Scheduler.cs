﻿using System;
using System.Timers;
using RomBerSoft.Logging;

namespace Storozhyk.com
{
    public class Scheduler
    {
        private Timer _timer;
        private int _count;
        private uint _nextSlideId;
        private byte[] _currentSlide;
        private uint _nextTimeForChangeSlide;

        public Scheduler()
        {
            _timer = new Timer(60000);
            _timer.Elapsed += Timer_Elapsed;
            _timer.Start();
            LoadNextSlide();
            _nextTimeForChangeSlide = 1440 - (uint)DateTime.Now.TimeOfDay.TotalMinutes;
        }

        void Timer_Elapsed (object sender, ElapsedEventArgs e)
        {
            _count++;
            if (_count % 1440 == _nextTimeForChangeSlide)
            {
                LoadNextSlide();
            }
        }

        public byte[] CurrentSlide
        {
            get { return _currentSlide; }
        }

        private void LoadNextSlide()
        {
            byte[] buff = null;
            Settings settings = null;
            SettingsDataStorage storage = new SettingsDataStorage();
            if (_nextSlideId == 0)
            {
                try
                {
                    settings = storage.Load<Settings>();
                }
                catch (ObjectNotFoundException ex)
                {
                    Logs.Error(ex.ToString());
                }
                if (settings == null)
                {
                    try
                    {
                        buff = new SlidesDataStorage().LoadFirstSlide(out _nextSlideId);
                        _currentSlide = buff;
                    }
                    catch (ObjectNotFoundException ex)
                    {
                        Logs.Error(ex.ToString());
                        return;
                    }
                    settings = new Settings();
                    settings.LastSlideIndex = _nextSlideId;
                    storage.Save(settings);
                    return;
                }
                else
                {
                    _nextSlideId = settings.LastSlideIndex;
                    if (!TryLoadNextSlide())
                    {
                        if (_currentSlide == null)
                        {
                            try
                            {
                                buff = new SlidesDataStorage().LoadFirstSlide(out _nextSlideId);
                                _currentSlide = buff;
                            }
                            catch (ObjectNotFoundException ex)
                            {
                                Logs.Error(ex.ToString());
                                return;
                            }
                            settings.LastSlideIndex = _nextSlideId;
                            storage.Save(settings);
                        }
                        return;
                    }
                }
            }
            else if (!TryLoadNextSlide()) return;
            if (settings == null)
            {
                try
                {
                    settings = storage.Load<Settings>();
                }
                catch (ObjectNotFoundException ex)
                {
                    Logs.Error(ex.ToString());
                    return;
                }
            }
            settings.LastSlideIndex = _nextSlideId;
            storage.Save(settings);
        }

        private bool TryLoadNextSlide()
        {
            byte[] buff = null;
            try
            {
                buff = new SlidesDataStorage().LoadNextSlide(ref _nextSlideId);
                _currentSlide = buff;
                return true;
            }
            catch(Exception ex)
            {
                Logs.Error(ex.ToString());
                return false;
            }
        }
    }
}

