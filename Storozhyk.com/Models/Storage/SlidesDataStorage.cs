﻿using System;
using System.Data;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using RomBerSoft.Logging;
using System.Text;

namespace Storozhyk.com
{
    public class SlidesDataStorage
    {
        private MySqlProvider _dbProvider;

        public SlidesDataStorage()
        {
            _dbProvider = new MySqlProvider();
        }

        public bool Save(byte[] buff1, byte[] buff2)
        {
            string sql = "INSERT INTO Slides (Image, ImagePreView) VALUES(@param1, @param2)";
            MySqlParameter[] parametres = new MySqlParameter[2];
            parametres[0] = new MySqlParameter("@param1", buff1);
            parametres[1] = new MySqlParameter("@param2", buff2);
            long result = _dbProvider.UpdateInsertQuery(sql, parametres);
            if(result > 0) 
            {
                Logs.Info("Record are inderted/updated in Slides table");
                return true;
            }
            return false;
        }

        public byte[] LoadFirstSlide(out uint id)
        {
            id = 0;
            string sql = "SELECT Id, Image FROM Slides ORDER BY Id LIMIT 1";
            DataTable table = _dbProvider.GetTable(sql);
            foreach (DataRow row in table.Rows)
            {
                id = Convert.ToUInt32(row[0]);
                return (byte[])row[1];
            }
            throw new ObjectNotFoundException("Table <Slides> is empty");
        }

        public byte[] LoadNextSlide(ref uint currentSlide)
        {
            string sql = string.Format("SELECT Id, Image FROM Slides WHERE Id > {0} ORDER BY Id LIMIT 1", currentSlide);
            DataTable table = _dbProvider.GetTable(sql);
            foreach (DataRow row in table.Rows)
            {
                currentSlide = Convert.ToUInt32(row[0]);
                return (byte[])row[1];
            }
            throw new ObjectNotFoundException("Current slide is last");
        }

        public byte[] LoadSlideById(uint id)
        {
            string sql = string.Format("SELECT Image FROM Slides WHERE Id = {0}", id);
            DataTable table = _dbProvider.GetTable(sql);
            foreach (DataRow row in table.Rows)
            {
                return (byte[])row[0];
            }
            throw new ObjectNotFoundException("Slide with Id = " + id + " does not exist");
        }

        public Slide[] LoadPreViews()
        {
            string sql = "SELECT Id, ImagePreView FROM Slides";
            DataTable table = _dbProvider.GetTable(sql);
            if (table.Rows.Count > 0)
            {
                List<Slide> list = new List<Slide>();
                foreach (DataRow row in table.Rows)
                {
                    list.Add(new Slide(Convert.ToUInt32(row[0]), (byte[])row[1]));
                }
                return list.ToArray();
            }
            throw new ObjectNotFoundException("Slides do not exist");
        }

        public long Remove(uint[] iDs)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("DELETE FROM Slides WHERE Id = ");
            builder.Append(iDs[0]);
            for (uint i = 1; i < iDs.Length; i++)
            {
                builder.Append(" OR Id = ");
                builder.Append(iDs[i]);
            }
            long result = _dbProvider.DeleteQuery(builder.ToString());
            Logs.Info("{0} records are deleted from Slides", result);
            return result;
        }
    }
}

