﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Globalization;
using RomBerSoft.Logging;
using Newtonsoft.Json;

namespace Storozhyk.com
{
    public class BlogsDataStorage
    {
        private MySqlProvider _dbProvider;

        public BlogsDataStorage()
        {
            _dbProvider = new MySqlProvider();
        }

        public bool Store(Blog blog)
        {
            string sql;
            if (blog.Id == 0)
                sql = string.Format("INSERT INTO Blogs (Title, Text, IdMainImage, Date, IdPictures, PreviewText, Video) VALUES ('{0}', '{1}', {2}, now(), '{3}', '{4}', '{5}')", 
                    blog.Title, blog.Text, blog.IdMainImage, JsonConvert.SerializeObject(blog.Images), blog.PreviewText, blog.Video);
            else
                sql = string.Format("UPDATE Blogs SET Title = '{0}', Text = '{1}', IdMainImage = {2}, Date = now(), IdPictures = '{3}', PreviewText = '{4}', Video = '{5}'",
                    blog.Title, blog.Text, blog.IdMainImage, JsonConvert.SerializeObject(blog.Images), blog.PreviewText, blog.Video);
            return _dbProvider.UpdateInsertQuery(sql, null) > 0;
        }

        public Blog[] LoadPreViews()
        {
            string sql = "SELECT * FROM Blogs";
            return GetBlogsFromDataTable(_dbProvider.GetTable(sql),  true);
        }

        public Blog Load(ushort id)
        {
            string sql = "SELECT * FROM Blogs WHERE Id = " + id;
            return GetBlogsFromDataTable(_dbProvider.GetTable(sql), false)[0];
        }

        private Blog[] GetBlogsFromDataTable(DataTable table, bool preview)
        {
            List<Blog> list = new List<Blog>();
            foreach (DataRow row in table.Rows)
            {
                Blog blog = new Blog();
                blog.Id = Convert.ToUInt16(row[0]);
                blog.Title = row[1].ToString();
                blog.Date = Convert.ToDateTime(row[4]).ToString("D", CultureInfo.GetCultureInfo("en-US"));
                if (!preview)
                {
                    blog.Text = row[2].ToString();
                    blog.Images = JsonConvert.DeserializeObject<uint[]>(row[5].ToString());
                    if(!(row[7] is DBNull) && !string.IsNullOrEmpty(row[7].ToString())) blog.Video = row[7].ToString();
                }
                else
                {
                    blog.IdMainImage = Convert.ToUInt32(row[3]);
                    blog.PreviewText = row[6].ToString();
                }
                list.Add(blog);
            }
            if (list.Count == 0) return null;
            return list.ToArray();
        }

        public void Remove(ushort id)
        {
            Blog blog = Load(id);
            if(blog == null) return;
            string sql = "DELETE FROM Blogs WHERE Id = " + id;
            long result = _dbProvider.DeleteQuery(sql);
            if (result > 0)
            {
                Logs.Info("Record with Id = {0} was deleted from table Blogs", id);
                PicturesDataStorage picStorage = new PicturesDataStorage();
                foreach (uint idImage in blog.Images)
                {
                    picStorage.Remove(idImage);
                }
            }
            else Logs.Info("Record with Id = {0} was not deleted from table Blogs", id);
        }
    }
}

