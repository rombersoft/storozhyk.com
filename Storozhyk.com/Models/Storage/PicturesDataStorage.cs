﻿using System;
using System.Collections.Generic;
using System.Data;
using MySql.Data.MySqlClient;
using RomBerSoft.Logging;

namespace Storozhyk.com
{
    public class PicturesDataStorage
    {
        private MySqlProvider _dbProvider;

        public PicturesDataStorage()
        {
            _dbProvider = new MySqlProvider();
        }

        public uint Store(byte[] image1000, byte[] image800, byte[] image600)
        {
            string sql = "INSERT INTO Images (Body1000, Body800, Body600) VALUES (@param1, @param2, @param3)";
            MySqlParameter[] parametres = new MySqlParameter[3];
            parametres[0] = new MySqlParameter("@param1", image1000);
            parametres[1] = new MySqlParameter("@param2", image800);
            parametres[2] = new MySqlParameter("@param3", image600);
            return (uint)_dbProvider.InsertQueryAndGetId(sql, parametres);
        }

        public void Store(Picture pic)
        {
            string sql = "Update Images SET Body1000 = @param1, Body800 = @param2, Body600 = @param3 WHERE Id = " + pic.Id;
            MySqlParameter[] parametres = new MySqlParameter[3];
            parametres[0] = new MySqlParameter("@param1", pic.Body1000);
            parametres[1] = new MySqlParameter("@param2", pic.Body800);
            parametres[2] = new MySqlParameter("@param3", pic.Body600);

            /*string sql = "Update Images SET Body800 = @param1, Body600 = @param2 WHERE Id = " + pic.Id;
            MySqlParameter[] parametres = new MySqlParameter[2];
            parametres[0] = new MySqlParameter("@param1", pic.Body800);
            parametres[1] = new MySqlParameter("@param2", pic.Body600);*/
            _dbProvider.UpdateInsertQuery(sql, parametres);
        }

        public byte[] LoadImage(uint id, int width)
        {
            string sql = "SELECT Body" + width + " FROM Images WHERE Id = " + id;
            return (byte[])_dbProvider.ScalarQuery(sql);
        }
        
        public Picture[] LoadImages(uint[] iDs)
        {
            string sql = "SELECT * FROM Images WHERE Id IN (" + string.Join(",",iDs) + ")";
            DataTable table = _dbProvider.GetTable(sql);
            List<Picture> list = new List<Picture>(iDs.Length);
            foreach (DataRow row in table.Rows)
            {
                list.Add(new Picture(Convert.ToUInt32(row[0]), (byte[])row[1], (byte[])row[2], (byte[])row[3]));
            }
            return list.ToArray();
        }

        public Picture[] LoadImages(uint[] iDs, int width)
        {
            string sql = "SELECT Id, Body" + width + " FROM Images WHERE Id IN (" + string.Join(",",iDs) + ")";
            DataTable table = _dbProvider.GetTable(sql);
            List<Picture> list = new List<Picture>(iDs.Length);
            Picture pic;
            foreach (DataRow row in table.Rows)
            {
                pic = new Picture(Convert.ToUInt32(row[0]), (byte[])row[1]);
                list.Add(pic);
            }
            return list.ToArray();
        }

        public Picture[] LoadAllImages()
        {
            string sql = "SELECT Id, Body1000 FROM Images";
            DataTable table = _dbProvider.GetTable(sql);
            List<Picture> list = new List<Picture>(100);
            foreach (DataRow row in table.Rows)
            {
                list.Add(new Picture(Convert.ToUInt32(row[0]), (byte[])row[1], (byte[])row[2], (byte[])row[3]));
            }
            return list.ToArray();
        }

        public void Remove(uint id)
        {
            string sql = "DELETE FROM Images WHERE Id = " + id;
            long result = _dbProvider.DeleteQuery(sql);
            if(result > 0) Logs.Info("Record with Id = {0} was deleted from table Images", id);
            else Logs.Info("Record with Id = {0} was not deleted from table Images", id);
        }
    }
}