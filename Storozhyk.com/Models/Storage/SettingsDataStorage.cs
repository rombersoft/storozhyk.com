﻿using System;
using System.Linq;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using RomBerSoft.Logging;

namespace Storozhyk.com
{
    public class SettingsDataStorage
    {
        private MySqlProvider _dbProvider;

        public SettingsDataStorage()
        {
            _dbProvider = new MySqlProvider();
        }

        public T Load<T>()
        {
            string[] array = typeof(T).Name.Split('.');
            string key = array.Last();
            string sql = string.Format("SELECT Body FROM Settings WHERE `Key` = '{0}'", key);
            object obj = _dbProvider.ScalarQuery(sql);
            if (obj == null)
                throw new ObjectNotFoundException(string.Format("Object {0} is not found", key));
            else
                return JsonConvert.DeserializeObject<T>(obj.ToString());
        }

        public void Save<T>(T data)
        {
            string key = typeof(T).Name;
            string sql = "SELECT Count(*) FROM Settings WHERE `Key` = '" + key + "'";
            object obj = _dbProvider.ScalarQuery(sql);
            if ((long)obj == 0)
            {
                sql = string.Format("INSERT INTO Settings(`Key`,Body) VALUES('{0}', '{1}')", key, data.ToString());
            }
            else
            {
                sql = string.Format("UPDATE Settings SET Body = '{0}' WHERE `Key` = '{1}'", data.ToString(), key);
            }
            long result = _dbProvider.UpdateInsertQuery(sql, null);
            if(result > 0) Logs.Info("Record {0} was inserted/updated in table Settings", key);
        }
    }
}

