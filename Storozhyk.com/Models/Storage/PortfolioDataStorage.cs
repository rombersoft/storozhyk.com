﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Globalization;
using RomBerSoft.Logging;
using Newtonsoft.Json;

namespace Storozhyk.com
{
    public class PortfolioDataStorage
    {
        private MySqlProvider _dbProvider;

        public PortfolioDataStorage()
        {
            _dbProvider = new MySqlProvider();
        }

        public bool Store(Portfolio portfolio)
        {
            string sql;
            if (portfolio.Id == 0)
                sql = string.Format("INSERT INTO Portfolio (Title, Text, IdMainImage, Date, IdPictures, Video) VALUES ('{0}', '{1}', {2}, now(), '{3}', '{4}')", 
                    portfolio.Title, portfolio.Text, portfolio.IdMainImage, JsonConvert.SerializeObject(portfolio.Images), portfolio.Video);
            else
                sql = string.Format("UPDATE Portfolio SET Title = '{0}', Text = '{1}', IdMainImage = {2}, Date = now(), IdPictures = '{3}', Video = '{4}'",
                    portfolio.Title, portfolio.Text, portfolio.IdMainImage, JsonConvert.SerializeObject(portfolio.Images), portfolio.Video);
            return _dbProvider.UpdateInsertQuery(sql, null) > 0;
        }

        public Portfolio[] LoadPreViews()
        {
            string sql = "SELECT * FROM Portfolio";
            return GetPortfolioFromDataTable(_dbProvider.GetTable(sql),  true);
        }

        public Portfolio Load(ushort id)
        {
            string sql = "SELECT * FROM Portfolio WHERE Id = " + id;
            return GetPortfolioFromDataTable(_dbProvider.GetTable(sql), false)[0];
        }

        private Portfolio[] GetPortfolioFromDataTable(DataTable table, bool preview)
        {
            List<Portfolio> list = new List<Portfolio>();
            foreach (DataRow row in table.Rows)
            {
                Portfolio portfolio = new Portfolio();
                portfolio.Id = Convert.ToUInt16(row[0]);
                portfolio.Title = row[1].ToString();
                portfolio.Date = Convert.ToDateTime(row[4]).ToString("D", CultureInfo.GetCultureInfo("en-US"));
                if (!preview)
                {
                    portfolio.Text = row[2].ToString();
                    portfolio.Images = JsonConvert.DeserializeObject<uint[]>(row[5].ToString());
                    if(!(row[6] is DBNull) && !string.IsNullOrEmpty(row[6].ToString())) portfolio.Video = row[6].ToString();
                }
                else portfolio.IdMainImage = Convert.ToUInt32(row[3]);
                list.Add(portfolio);
            }
            if (list.Count == 0) return null;
            return list.ToArray();
        }

        public void Remove(ushort id)
        {
            Portfolio portfolio = Load(id);
            if(portfolio == null) return;
            string sql = "DELETE FROM Portfolio WHERE Id = " + id;
            long result = _dbProvider.DeleteQuery(sql);
            if (result > 0)
            {
                Logs.Info("Record with Id = {0} was deleted from table Portfolio", id);
                PicturesDataStorage picStorage = new PicturesDataStorage();
                foreach (uint idImage in portfolio.Images)
                {
                    picStorage.Remove(idImage);
                }
            }
            else Logs.Info("Record with Id = {0} was not deleted from table Portfolio", id);
        }
    }
}