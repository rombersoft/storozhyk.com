﻿using System;
using System.Data;
using System.Web.Configuration;
using MySql.Data.MySqlClient;
using RomBerSoft.Logging;
//using System.Configuration;

namespace Storozhyk.com
{
    public class MySqlProvider
    {
        private static string _connectionString = WebConfigurationManager.ConnectionStrings["InetConnection"].ConnectionString;
        //private static string _connectionString = ConfigurationManager.ConnectionStrings["InetConnection"].ConnectionString;

        private void CreateConnection(out MySqlConnection connection, out MySqlCommand command)
        {
            Console.WriteLine(Environment.StackTrace);
            for (byte i = 0; i < 2; i++)
            {
                connection = new MySqlConnection(_connectionString);
                try
                {
                    connection.Open();
                    command = connection.CreateCommand();
                    return;
                }
                catch (Exception e)
                {
                    Logs.Error("Ошибка подключения к базе {0}", e.Message);
                }
            }
            throw new Exception("Не удалось открыть подключение к базе");
        }

        public long UpdateInsertQuery(string sql, MySqlParameter[] parameters)
        {
            MySqlConnection connection;
            MySqlCommand command;
            CreateConnection(out connection, out command);
            MySqlTransaction transaction = connection.BeginTransaction();
            command.CommandText = sql;
            if (parameters != null) command.Parameters.AddRange(parameters);
            int result = 0;
            try
            {
                result = command.ExecuteNonQuery();
                transaction.Commit();
            }
            catch(Exception ex)
            {
                Logs.Error("Ошибка {0} выполнения SQL запроса {1}", ex.Message, sql);
                if(transaction.Connection.State == ConnectionState.Open) transaction.Rollback();
            }
            finally
            {
                command.Dispose();
                connection.Close();
            }
            return result;
        }

        public long InsertQueryAndGetId(string sql, MySqlParameter[] parameters)
        {
            MySqlConnection connection;
            MySqlCommand command;
            CreateConnection(out connection, out command);
            MySqlTransaction transaction = connection.BeginTransaction();
            command.CommandText = sql;
            if (parameters != null) command.Parameters.AddRange(parameters);
            long result = 0;
            try
            {
                command.ExecuteNonQuery();
                result = command.LastInsertedId;
                transaction.Commit();
            }
            catch (Exception ex)
            {
                Logs.Error("Ошибка {0} выполнения SQL запроса {1}", ex.Message + " " + ex.StackTrace, sql);
                if (transaction.Connection.State == ConnectionState.Open) transaction.Rollback();
            }
            finally
            {
                command.Dispose();
                connection.Close();
            }
            return result;
        }

        public void TruncateTable(string table)
        {
            MySqlConnection connection;
            MySqlCommand command;
            CreateConnection(out connection, out command);
            MySqlTransaction transaction = connection.BeginTransaction();
            command.CommandText = "TRUNCATE " + table;
            try
            {
                command.ExecuteNonQuery();
                transaction.Commit();
            }
            catch (Exception ex)
            {
                Logs.Error("Ошибка {0} выполнения SQL запроса {1}", ex.Message + " " + ex.StackTrace, "TRUNCATE " + table);
                if (transaction.Connection.State == ConnectionState.Open) transaction.Rollback();
                throw new Exception(ex.Message);
            }
            finally
            {
                command.Dispose();
                connection.Close();
            }
        }

        public long DeleteQuery(string sql)
        {
            MySqlConnection connection;
            MySqlCommand command;
            CreateConnection(out connection, out command);
            command.CommandText = sql;
            int result = 0;
            try
            {
                result = command.ExecuteNonQuery();
            }
            catch(Exception ex)
            {
                Logs.Error("Ошибка {0} выполнения SQL запроса {1}", ex.Message, sql);
            }
            finally
            {
                command.Dispose();
                connection.Close();
            }
            return result;
        }

        public object ScalarQuery(string sql)
        {
            MySqlConnection connection;
            MySqlCommand command;
            CreateConnection(out connection, out command);
            command.CommandText = sql;
            object val = null;
            try
            {
                val = command.ExecuteScalar();
            }
            finally
            {
                command.Dispose();
                connection.Close();
            }
            return val;
        }

        public DataTable GetTable(string sql)
        {
            MySqlConnection connection;
            MySqlCommand command;
            CreateConnection(out connection, out command);
            command.CommandText = sql;
            var table = new DataTable();
            table.TableName = "1";
            var p = new MySqlDataAdapter(command);
            try
            {
                p.Fill(table);
            }
            finally
            {
                command.Dispose();
                connection.Close();
            }
            return table;
        }
    }
}

