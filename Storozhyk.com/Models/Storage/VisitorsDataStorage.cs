﻿using System;
using RomBerSoft.Logging;
using System.Data;
using System.Collections.Generic;

namespace Storozhyk.com
{
    public class VisitorsDataStorage
    {
        MySqlProvider _dbProvider;

        public VisitorsDataStorage()
        {
            _dbProvider = new MySqlProvider();
        }

        public void Save(Visitor visitor)
        {
            string sql = string.Format("INSERT INTO Visitors(IP, `Date`, WebEngine) VALUES('{0}', now(), '{1}')", visitor.IP, visitor.WebEngine);
            long result = _dbProvider.UpdateInsertQuery(sql, null);
            if (result > 0)
                Logs.Info("Record was inserted/updated in table Visitors");
        }

        public Visitor[] LoadAll()
        {
            string sql = "SELECT * FROM Visitors";
            DataTable table = _dbProvider.GetTable(sql);
            if (table.Rows.Count > 0)
            {
                List<Visitor> visirors = new List<Visitor>();
                foreach (DataRow row in table.Rows)
                {
                    visirors.Add(new Visitor(row[0].ToString(), row[1].ToString(), row[2].ToString()));
                }
                return visirors.ToArray();
            }
            return new Visitor[0];
        }
    }
}

