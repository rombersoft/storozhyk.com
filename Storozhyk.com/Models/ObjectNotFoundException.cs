﻿using System;

namespace Storozhyk.com
{
    public class ObjectNotFoundException : Exception
    {
        public ObjectNotFoundException(string message) : base( message )
        {
            
        }
    }
}

