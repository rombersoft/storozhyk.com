﻿using System;
using Storozhyk.com;
using System.IO;

namespace Assistent
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            PicturesDataStorage storage = new PicturesDataStorage();
            Picture[] pictures = storage.LoadAllImages();
            foreach (Picture pic in pictures)
            {
                using (MemoryStream stream = new MemoryStream(pic.AloneBody))
                {
                    pic.Body800 = Commons.Instance.RemakeImg(stream, null, 800);
                    pic.Body600 = Commons.Instance.RemakeImg(stream, null, 600);
                }
                storage.Store(pic);
                Console.WriteLine(pic.Id);
            }
        }
    }
}
